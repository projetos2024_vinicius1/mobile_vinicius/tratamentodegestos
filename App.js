import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import CountMoviment from "./src/countMoviment";
import ImageMoviment from "./src/imageMoviment";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="ImageMoviment">
        <Stack.Screen name="CountMoviment" component={CountMoviment}/>
        <Stack.Screen name="ImageMoviment" component={ImageMoviment}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

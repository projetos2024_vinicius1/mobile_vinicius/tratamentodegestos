import React, { useRef, useState } from "react";
import { View, Image, Dimensions, PanResponder, StyleSheet, Text } from "react-native";
import Roberto from "../assets/Roberto.jpg";
import Coca from "../assets/coca.webp";
import CaveiraLendaria from "../assets/caveiraLendaria.webp";

const ImageMoviment = () => {
    const [count, setCount] = useState(0);
    const screenWidth = Dimensions.get("window").width;
    const gestureThreshold = screenWidth * 0.5;

    const panResponder = useRef(
        PanResponder.create({
            onStartShouldSetPanResponder: () => true,
            onPanResponderMove: (event, gestureState) => { },
            onPanResponderRelease: (event, gestureState) => {
                if (gestureState.dx < -gestureThreshold) {
                    // setCount(prevCount => Math.min(prevCount + 1, images.length - 1));
                    setCount(prevCount => (prevCount + 1) % images.length);
                } 
                else if (gestureState.dx > gestureThreshold) {
                    // setCount(prevCount => Math.max(prevCount - 1, 0));
                    setCount(prevCount => (prevCount - 1 + images.length) % images.length);
                }
            },
        })
    ).current;

    const images = [Roberto, Coca, CaveiraLendaria];

    return (
        <View {...panResponder.panHandlers} style={styles.container}>
            <Image
                source={images[count]}
                style={styles.image}
                resizeMode="contain"
            />
            <Text>Valor: {count}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    image: {
        width: "75%",
        height: "75%",
    },
});

export default ImageMoviment;